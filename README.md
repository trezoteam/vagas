## O que oferecemos

- Contrato CLT com salário compatível com a experiência
- Plano de Saude
- Plano ondontológico
- Contratação Imediata
- Ambiente descontraído e inovador
- Incentivo para participação em eventos ou aquisição de livros/cursos
- TechTalks realizados pela equipe, semanalmente
- Bonificação por resultados e metas cumpridas

## Quem somos?
- www.trezo.com.br
- A Trezo trabalha exclusivamente com e-commerce. É premissa que os candidatos queiram viver e respirar esse mundo de vendas online.
- Formada em 2009, a Trezo veio para suprir a carência do mercado na plataforma **Magento** unindo qualidade, segurança, agilidade e flexibilidade, focada na conversão da sua loja Magento. Ambiente descontraído e equipe capacitada, tornou a Trezo referência nacional quando se fala em e-commerce na plataforma Magento.
- Blog do nosso time compartilhando conhecimento: https://medium.com/trezoteam

## Que fazer parte da família #TrezoTeam?

Se você se interessou pelas vagas descritas, mande um e-mail com seu currículo/conta no Github para **vagas@trezo.com.br** com a descrição da vaga de interesse.