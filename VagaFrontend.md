# Desenvolvedor Frontend

## Requisitos

- Conhecimento prático em HTML5 e CSS3 Responsivo
- Conhecimento avançado de JavaScript
- Desenvolvimento cross-plataforma e testes cross-browser
- Confortável trabalhando com o controle de versão (Git)
- Experiência de trabalho com APIs, ou integrar uma API de terceiros ou criar o seu próprio, quando necessário.
- Conhecer pré-processadores CSS como Less, Sass;
- Experiência com UI e UX;
- Conhecimento básico de banco de dados (Mysql, Postgres)
- Morar ou estar disposto a mudar-se para Blumenau/SC


## Requisitos adicionais

- Conhecer frameworks JS como e AngularJS, React ou VueJS;
- Conhecer frameworks PHP (Yii, Laravel, Zend, etc);
- Conhecimento básico da plataforma/desenvolvimento com Magento;
- Experiência com sistemas de build (Bash, Grunt, Make, Ant, Gulp, Webpack, NPM);
- Conhecimento básico em pelo menos uma linguagem de programação orientada a objetos (ex: PHP, Java ou Python);
- Conhecimento básico com ambiente linux, docker, nginx/apache;


## Perfil esperado

- Disciplina, comprometimento e organização é fundamental;
- Sedento por conhecimento;
- Traz críticas em relação a ferramentas utilizadas, com o objetivo de melhorar o desenvolvimento;
- Pesquisador nas horas vagas, trazendo novidades que podem ser realmente aplicadas nos projetos;
- Senso crítico;
- Engajamento com a equipe, para crescimento sustentável de todos;
- Disposto a qualquer tipo de desafio;
- Disposto a compartilhar com a comunidade seu aprendizado e soluções (stack overflow, Github, e no criar artigos no nosso Medium: https://medium.com/trezoteam)

## O que oferecemos

- Contrato CLT com salário compatível com a experiência
- Plano de Saude
- Plano ondontológico
- Contratação Imediata
- Ambiente descontraído e inovador
- Incentivo para participação em eventos ou aquisição de livros/cursos
- TechTalks realizados pela equipe, semanalmente
- Bonificação por resultados e metas cumpridas

## Sobre a Trezo
- www.trezo.com.br
- A Trezo trabalha exclusivamente com e-commerce. É premissa que os candidatos queiram viver e respirar esse mundo de vendas online.
- Formada em 2009, a Trezo veio para suprir a carência do mercado na plataforma **Magento** unindo qualidade, segurança, agilidade e flexibilidade, focada na conversão da sua loja Magento. Ambiente descontraído e equipe capacitada, tornou a Trezo referência nacional quando se fala em e-commerce na plataforma Magento.

## Que fazer parte da família #TrezoTeam?

Se você atende os requisitos acima mande um e-mail com seu currículo/conta no Github para **vagas@trezo.com.br**